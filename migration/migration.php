<?php
require_once '../config.php';

// Vérifie si le script est exécuté en ligne de commande
if (php_sapi_name() !== 'cli') {
  echo "Ce script ne peut être exécuté qu'en ligne de commande.";
  die;
}

// Vérifie si le nombre de paramètres est correct
if ($argc !== 3) {
  echo "Nombre de paramètres attendus incorrect ! : 2 paramètres attendus";
  die;
}

// Récupère le nom du fichier SQL à migrer depuis les arguments de la ligne de commande
$fileNameSQL = $argv[1];
// Récupère le nom de la base de données à créer
$dbName = $argv[2];

// connexion au serveur mySQL
$pdo = new PDO("mysql:host=". DB_HOST, DB_USER, DB_PASSWORD);

$request = "CREATE DATABASE IF NOT EXISTS $dbName";

// Vérifie si le nom de la base de données n'existe pas déjà
if($pdo->exec($request) === false) {
  echo "La base de données $dbName existe déjà !";
  die;
}

// Lire le contenu du fichier SQL
$sql = file_get_contents($fileNameSQL);
// Connexion à la base de données
$pdo = new PDO("mysql:host=". DB_HOST . ";dbname=" . $dbName, DB_USER, DB_PASSWORD);

// Exécute le contenu du fichier SQL
if ($pdo->exec($sql) === false) {
  echo "Erreur de migration : " . $fileNameSQL . " ne s'est pas éxécuté !";
  die;
}

echo "La migration a réussie : $fileNameSQL a été importé dans la base de données.";

$pdo = null;